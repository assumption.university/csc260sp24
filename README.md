# CSC260 Spring 2014 Project

This repository is used for demonstration of using `git` 

To start, `clone` this repository to your local machine

```
$ git clone https://gitlab.com/assumption.university/csc260sp
```

Then follow the instructions that you have learned from our classes

Goodluck

Benz.
