
/*adding some comments*/

#include <iostream>
using namespace std;

int main(int argc, char* argv[]) {
    int n = atoi(argv[1]);

    if (n<1){
        cout << "Invalid value of N.";
        return 1;
    }
    int a1 = 1, a2 = 1;
    if (n<=2){
        cout << 1;
    } else {
        int i;
        for(i = 3; i <=n; i++){
            int tmp = a1 + a2;
            a1 = a2;
            a2 = tmp;
        }
        cout << a2;
    }

    return 0;

}
